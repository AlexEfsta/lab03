//Alexander Efstathakis 2043441
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;
public class Vector3dTests {
    // this test will fail
    
    @Test
    public void getVector3d() {
        Vector3d test = new Vector3d(1, 1, 2);
        assertEquals(1, test.getX());
        assertEquals(1, test.getY());
        assertEquals(2, test.getZ());
    }

    @Test
    public void vectorMagnitude() {
        Vector3d test2 = new Vector3d(1, 2, 2);
        assertEquals(3, test2.magnitude());
    }

    @Test
    public void productVector() {
        Vector3d first = new Vector3d(2, 3, 4);
        assertEquals(16, first.dotProduct(1, 2, 2));
    }

    @Test
    public void addVector() {
        Vector3d test = new Vector3d(1, 2, 2);
        Vector3d test2 = new Vector3d(2, 3, 4);
        test = test.add(test2);
        assertEquals(3.0, test.getX());
        assertEquals(5.0, test.getY());
        assertEquals(6.0, test.getZ());
        }
    }


