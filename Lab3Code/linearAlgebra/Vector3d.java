//Alexander Efstathakis 2043441
import java.lang.Math;
public class Vector3d {
    private double x;
    private double y;
    private double z;
    
    public Vector3d (double first, double second, double third){
        this.x = first;
        this.y = second;
        this.z = third;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        double magnitude = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
        return magnitude;
    }

    public double dotProduct(double a, double b, double c){
        double product = (this.x * a) + (this.y * b) + (this.z * c);
        return product;
    }

    public Vector3d add(Vector3d foo){
        Vector3d sum = new Vector3d(this.x , this.y, this.z);
        double first = this.x + foo.getX();
        double second = this.y + foo.getY();
        double third = this.z + foo.getZ();
        Vector3d finish = new Vector3d(first, second, third);
        return finish;
    }
}